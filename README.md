# Roperty REST

A REST service to allow the client interacting with Roperty

## Building

To build the Roperty REST service, run:

    mvn clean package

To ensure everything worked, run:

    java -jar target/*.jar

## Continuous Integration

[![Linux/Mac Build Status](https://secure.travis-ci.org/dheid/roperty-rest.png)](http://travis-ci.org/dheid/roperty-rest)
[![Windows Build Status](https://img.shields.io/appveyor/ci/dheid/roperty-rest/master.svg?label=windows)](https://ci.appveyor.com/project/dheid/roperty-rest/branch/master)
[![Coverage Status](https://coveralls.io/repos/dheid/roperty-rest/badge.svg?branch=master&service=gitlab)](https://coveralls.io.github/dheid/roperty-rest?branch=master)
[![Maven Central](https://img.shields.io/maven-central/v/io.github.dheid/roperty-rest.svg?maxAge=2592000)](http://search.maven.org/#search%7Cgav%7C1%7Cg%3A%22io.github.dheid%22%20AND%20a%3A%22roperty-rest%22)

To setup this project in Jenkins, use the following configuration:

* Project name: `roperty-rest`
* Source Code Management
    * Git Repository: `git@gitlab.com:dheid/roperty-rest.git`
    * Branches to build: `*/master`
    * Additional Behaviours: `Wipe out repository & force clone`
* Build Triggers
    * Poll SCM / Schedule: `H/5 * * * *`
* Build
    * Invoke Maven / Tasks: `clean package`
* Post-build Actions
    * Publish JUnit test result report / Test Report XMLs: `target/surefire-reports/*.xml`
