package com.parshipelite.roperty_rest.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(prefix = "roperty-rest", ignoreUnknownFields = false)
public class RopertyRestProperties {

    private List<String> domains = new ArrayList<>();

    public List<String> getDomains() {
        return domains;
    }
}
