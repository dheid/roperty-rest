package com.parshipelite.roperty_rest.config;

import com.mongodb.MongoClient;
import com.parship.roperty.Persistence;
import com.parship.roperty.Roperty;
import com.parship.roperty.RopertyImpl;
import com.parship.roperty.mongodb.CollectionProvider;
import com.parship.roperty.mongodb.KeyBlockFactory;
import com.parship.roperty.mongodb.KeyDocumentDAO;
import com.parship.roperty.mongodb.MongoDbPersistence;
import com.parship.roperty.mongodb.RemoveOperationFactory;
import com.parship.roperty.mongodb.ValueAnalyzerFactory;
import com.parship.roperty.mongodb.ValueDocumentDAO;
import com.parship.roperty.mongodb.ValueDocumentTransformerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class RopertyConfiguration {

    @Autowired
    private RopertyRestProperties ropertyRestProperties;

    @Bean
    KeyDocumentDAO keyDocumentDAO() {
        KeyDocumentDAO keyDocumentDAO = new KeyDocumentDAO();
        keyDocumentDAO.setCollectionProvider(collectionProvider());
        return keyDocumentDAO;
    }

    @Bean
    Persistence persistence() {
        MongoDbPersistence persistence = new MongoDbPersistence();
        persistence.setCollectionProvider(collectionProvider());
        persistence.setKeyDocumentDAO(keyDocumentDAO());
        persistence.setValueDocumentDAO(valueDocumentDAO());
        persistence.setKeyBlockFactory(keyBlockFactory());
        persistence.setRemoveOperationFactory(removeOperationFactory());
        persistence.setValueDocumentTransformerFactory(valueDocumentTransformerFactory());
        persistence.setValueAnalyzerFactory(valueAnalyzerFactory());
        return persistence;
    }

    @Bean
    ValueAnalyzerFactory valueAnalyzerFactory() {
        return new ValueAnalyzerFactory();
    }

    @Bean
    ValueDocumentTransformerFactory valueDocumentTransformerFactory() {
        return new ValueDocumentTransformerFactory();
    }

    @Bean
    RemoveOperationFactory removeOperationFactory() {
        RemoveOperationFactory removeOperationFactory = new RemoveOperationFactory();
        removeOperationFactory.setCollectionProvider(collectionProvider());
        removeOperationFactory.setKeyDocumentDAO(keyDocumentDAO());
        removeOperationFactory.setValueDocumentDAO(valueDocumentDAO());
        return removeOperationFactory;
    }

    @Bean
    KeyBlockFactory keyBlockFactory() {
        KeyBlockFactory keyBlockFactory = new KeyBlockFactory();
        keyBlockFactory.setValueDocumentTransformerFactory(valueDocumentTransformerFactory());
        return keyBlockFactory;
    }

    @Bean
    ValueDocumentDAO valueDocumentDAO() {
        ValueDocumentDAO valueDocumentDAO = new ValueDocumentDAO();
        valueDocumentDAO.setCollectionProvider(collectionProvider());
        valueDocumentDAO.setValueAnalyzerFactory(valueAnalyzerFactory());
        return valueDocumentDAO;
    }

    @Bean
    CollectionProvider collectionProvider() {
        CollectionProvider collectionProvider = new CollectionProvider();
        collectionProvider.setCollectionName("properties");
        collectionProvider.setDatabaseName("roperty");
        collectionProvider.setMongo(mongo());
        return collectionProvider;
    }

    @Bean
    MongoClient mongo() {
        MongoClient mongoClient = new MongoClient();
        return mongoClient;
    }

    @Bean
    public Roperty roperty() {
        List<String> domains = ropertyRestProperties.getDomains();
        RopertyImpl roperty = new RopertyImpl(persistence(), domains.toArray(new String[domains.size()]));
        return roperty;
    }

}
