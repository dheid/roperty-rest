package com.parshipelite.roperty_rest.resources;

import com.parshipelite.roperty_rest.resources.response.KeyResponse;
import com.parshipelite.roperty_rest.service.KeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/keys", produces = MediaType.APPLICATION_JSON_VALUE)
public class KeyResource {

    @Autowired
    private KeyService keyService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<KeyResponse>> findKeys(@RequestParam String substring) {
        List<KeyResponse> keys = keyService
            .findKeys(substring)
            .stream()
            .map(KeyResponse::new)
            .collect(Collectors.toList());
        return new ResponseEntity<>(keys, HttpStatus.OK);
    }

}
