package com.parshipelite.roperty_rest.resources.response;

import com.parshipelite.roperty_rest.domain.Key;


public class KeyResponse {

    private final String name;

    public KeyResponse(Key key) {
        this.name = key.getName();
    }

    public String getName() {
        return name;
    }
}
