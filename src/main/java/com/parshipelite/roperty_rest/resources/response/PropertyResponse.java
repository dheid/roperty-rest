package com.parshipelite.roperty_rest.resources.response;

import com.parshipelite.roperty_rest.domain.Property;

public class PropertyResponse {

    private final Object value;

    public PropertyResponse(Property property) {
        this.value = property.getValue();
    }

    public Object getValue() {
        return value;
    }
}
