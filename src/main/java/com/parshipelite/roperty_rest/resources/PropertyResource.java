package com.parshipelite.roperty_rest.resources;

import com.parshipelite.roperty_rest.domain.Property;
import com.parshipelite.roperty_rest.resources.response.PropertyResponse;
import com.parshipelite.roperty_rest.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping(value = "/properties/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
public class PropertyResource {

    @Autowired
    private PropertyService propertyService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<PropertyResponse> getProperty(@PathVariable String key, @RequestParam(required = false) String... domainValues) {
        Property property = propertyService.findByKey(key, domainValues);
        return new ResponseEntity<>(new PropertyResponse(property), HttpStatus.OK);
    }

    @RequestMapping(method = PUT)
    public ResponseEntity<Void> updateProperty(@PathVariable String key, @Valid @RequestBody Property property) {
        propertyService.save(key, property);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
