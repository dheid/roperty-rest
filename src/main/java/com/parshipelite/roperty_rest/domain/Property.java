package com.parshipelite.roperty_rest.domain;

public class Property {

    private final Object value;

    public Property(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }
}
