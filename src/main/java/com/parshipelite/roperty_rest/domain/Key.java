package com.parshipelite.roperty_rest.domain;

public class Key {

    private String name;

    public Key(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
