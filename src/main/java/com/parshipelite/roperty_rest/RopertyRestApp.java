package com.parshipelite.roperty_rest;

import com.parshipelite.roperty_rest.config.RopertyRestProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(RopertyRestProperties.class)
public class RopertyRestApp {

    public static void main(String[] args) {
        SpringApplication.run(RopertyRestApp.class, args);
    }

}
