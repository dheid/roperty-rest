package com.parshipelite.roperty_rest.service;

import com.parship.roperty.MapBackedDomainResolver;
import com.parship.roperty.Roperty;
import com.parshipelite.roperty_rest.config.RopertyRestProperties;
import com.parshipelite.roperty_rest.domain.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PropertyService {

    @Autowired
    private Roperty roperty;

    @Autowired
    private RopertyRestProperties ropertyRestProperties;

    public Property findByKey(String key, String... domainValues) {
        MapBackedDomainResolver domainResolver = new MapBackedDomainResolver();
        List<String> domains = ropertyRestProperties.getDomains();
        for (int i = 0; i < domains.size(); i++) {
            domainResolver.set(domains.get(i), domainValues[i]);
        }
        Object value = roperty.get(key, domainResolver);
        return new Property(value);
    }

    public void save(String key, Property property, String... domains) {

        roperty.set(key, property.getValue(), "", domains);

    }
}
