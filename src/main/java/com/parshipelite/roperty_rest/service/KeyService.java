package com.parshipelite.roperty_rest.service;

import com.parship.roperty.Roperty;
import com.parshipelite.roperty_rest.domain.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class KeyService {

    @Autowired
    private Roperty roperty;

    public List<Key> findKeys(String substring) {
        return roperty.findKeys(substring)
            .stream()
            .map(Key::new)
            .collect(Collectors.toList());
    }

}
