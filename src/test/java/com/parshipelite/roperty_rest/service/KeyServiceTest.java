package com.parshipelite.roperty_rest.service;

import com.parship.roperty.Roperty;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * Created by daniel on 30.03.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class KeyServiceTest {

    @InjectMocks
    private KeyService keyService;

    @Mock
    private Roperty roperty;

    @Test
    public void translatesTwoWordsToRegEx() {
        String substring = "hello";
        keyService.findKeys(substring);
        verify(roperty).findKeys(substring);
    }

}
